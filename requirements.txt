bokeh==0.12.5
keras>=2.0.0
librosa
numpy
pandas
pydub
sklearn
sounddevice
tornado==4.3
tqdm
